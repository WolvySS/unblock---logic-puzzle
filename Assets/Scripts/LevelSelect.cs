﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelSelect : MonoBehaviour 
{
	public Button[] Levels;
	int levelPassed;

	void Start()
	{
		levelPassed = PlayerPrefs.GetInt ("LevelPassed");
		foreach (Button levelButton in Levels) {
			levelButton.interactable = false;
		}
		resetPlayerPrefs ();

		switch (levelPassed) {
		case 1:
			Levels [1].interactable = true;
			break;
		case 2:
			Levels [1].interactable = true;
			Levels [2].interactable = true;
			break;
		case 3:
			Levels [1].interactable = true;
			Levels [2].interactable = true;
			Levels [3].interactable = true;
			break;
		case 4:
			Levels [1].interactable = true;
			Levels [2].interactable = true;
			Levels [3].interactable = true;
			Levels [4].interactable = true;
			break;
		case 5:
			Levels [1].interactable = true;
			Levels [2].interactable = true;
			Levels [3].interactable = true;
			Levels [4].interactable = true;
			Levels [5].interactable = true;
			break;
		case 6:
			Levels [1].interactable = true;
			Levels [2].interactable = true;
			Levels [3].interactable = true;
			Levels [4].interactable = true;
			Levels [5].interactable = true;
			Levels [6].interactable = true;
			break;
		case 7:
			Levels [1].interactable = true;
			Levels [2].interactable = true;
			Levels [3].interactable = true;
			Levels [4].interactable = true;
			Levels [5].interactable = true;
			Levels [6].interactable = true;
			Levels [7].interactable = true;
			break;
		case 8:
			Levels [1].interactable = true;
			Levels [2].interactable = true;
			Levels [3].interactable = true;
			Levels [4].interactable = true;
			Levels [5].interactable = true;
			Levels [6].interactable = true;
			Levels [7].interactable = true;
			Levels [8].interactable = true;
			break;
		case 9:
			Levels [1].interactable = true;
			Levels [2].interactable = true;
			Levels [3].interactable = true;
			Levels [4].interactable = true;
			Levels [5].interactable = true;
			Levels [6].interactable = true;
			Levels [7].interactable = true;
			Levels [8].interactable = true;
			Levels [9].interactable = true;
			break;
		case 10:
			Levels [1].interactable = true;
			Levels [2].interactable = true;
			Levels [3].interactable = true;
			Levels [4].interactable = true;
			Levels [5].interactable = true;
			Levels [6].interactable = true;
			Levels [7].interactable = true;
			Levels [8].interactable = true;
			Levels [9].interactable = true;
			Levels [10].interactable = true;
			break;
		}
	}

	public void LoadLevel(string levelName)
	{
		SceneManager.LoadScene (levelName);
	}

	public void resetPlayerPrefs()
	{
		foreach (Button levels in Levels) {
			levels.interactable = false;
		}
		PlayerPrefs.DeleteAll ();
		Levels [0].interactable = true;
	}
}
