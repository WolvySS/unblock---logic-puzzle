﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeScript : MonoBehaviour 
{
	public BoxCollider2D blocks;
	private Touch touch;
	private Vector2 touchPos, startTouch, swipeDelta;
	private bool isDragging;
	private bool swipeLeft, swipeRight, swipeUp, swipeDown;

	public bool SwipeLeft{ get { return swipeLeft; } }

	public bool SwipeRight{ get { return swipeRight; } }

	public bool SwipeUp{ get { return swipeUp; } }

	public bool SwipeDown{ get { return swipeDown; } }

	void Start () 
	{
		blocks = GetComponent<BoxCollider2D> ();
	}
		
	void Update () 
	{
		swipeUp = swipeLeft = swipeDown = swipeRight = false;

		#region StandAlone Input          

		if (Input.GetMouseButtonDown (0)) 
		{
			Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			Vector2 mousePos2D = new Vector2 (mousePos.x, mousePos.y);

			Collider2D hit = Physics2D.OverlapPoint(mousePos2D);      // To find the object on mouse touch...
			if (hit==blocks)
			{
				isDragging = true;
				startTouch=Input.mousePosition;
			}
		} 
		else if (Input.GetMouseButtonUp (0))
		{
			isDragging = false;
			Reset ();
		}
		#endregion


		#region Mobile Inputs

		if (Input.touches.Length > 0) 
		{      
			touch = Input.GetTouch (0);
			touchPos = Camera.main.ScreenToWorldPoint (touch.position);   // To find the object we touch...
			if (Input.touches [0].phase == TouchPhase.Began)
			{
				if (blocks == Physics2D.OverlapPoint (touchPos)) 
				{
					isDragging = true;
				}
				startTouch = Input.touches [0].position; 
			} 
			else if (Input.touches [0].phase == TouchPhase.Ended || Input.touches [0].phase == TouchPhase.Canceled) 
			{
				isDragging = false;
				Reset ();
			}
		}
		#endregion

		// Calculating swipe  Distance...
		swipeDelta = Vector2.zero;
		if (isDragging) 
		{   
			if (Input.touches.Length > 0) 
			{
				swipeDelta = Input.touches [0].position - startTouch;
			}
			else if (Input.GetMouseButton (0)) 
			{
				swipeDelta = (Vector2)Input.mousePosition - startTouch;
			}
		}

		// To Find the Direction...
		if (swipeDelta.magnitude > 125) 
		{     
			float x = swipeDelta.x;
			float y = swipeDelta.y;

			if (Mathf.Abs (x) > Mathf.Abs (y)) 
			{
				if (x < 0)
					swipeLeft = true;
				else if (x > 0)                 
					swipeRight = true;
			} 
			else 
			{
				if (y < 0)
					swipeDown = true;
				else if (y > 0)
					swipeUp = true;
			}
			Reset ();          
		}
	}

	public void Reset ()
	{
		startTouch = swipeDelta = Vector2.zero;
		isDragging = false;
	}
}