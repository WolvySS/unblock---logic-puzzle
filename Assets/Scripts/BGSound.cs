﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGSound : MonoBehaviour {

	public AudioSource music;
	private static BGSound instance = null;

	public static BGSound Instance
	{
		get{ return instance; }
	}

	void Awake () 
	{
		if (instance != null && instance != this) 
		{
			Destroy (this.gameObject);
		} 
		else 
		{
			instance = this;
		}
		DontDestroyOnLoad (this.gameObject);
	}

	public void Music()
	{
		if (music.isPlaying)
			music.Pause ();
		else
			music.Play ();
	}
}
