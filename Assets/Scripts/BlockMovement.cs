﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockMovement : MonoBehaviour 
{
	public SwipeScript controls;
	private Vector3 desiredPosition,startPos,newPos;
	private bool swipeLeft, swipeRight, swipeUp, swipeDown;

	void Start()
	{
		startPos = controls.blocks.transform.position;
		desiredPosition = Vector3.zero;
	}

	void Update ()
	{
		newPos = startPos + desiredPosition;
		if (controls.SwipeLeft) {
			desiredPosition += (Vector3.left);
		} else if (controls.SwipeRight) {
			desiredPosition += Vector3.right;
		} else if (controls.SwipeUp) {
			desiredPosition += Vector3.up;
		} else if (controls.SwipeDown) {
			desiredPosition += Vector3.down;
		}

		//  Assigning the Direction of movement and Distance to be moved to the Touched object...
			controls.blocks.transform.position = Vector3.MoveTowards (controls.blocks.transform.position, newPos, 5f * Time.deltaTime);
	}
}