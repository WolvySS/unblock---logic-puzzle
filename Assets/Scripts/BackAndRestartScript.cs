﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackAndRestartScript : MonoBehaviour {

	public void BackButton()
	{
		SceneManager.LoadScene ("Main Menu");
	}

	public void RestartLevelButton()
	{
		Scene scene = SceneManager.GetActiveScene(); 
		SceneManager.LoadScene (scene.name);
	}
}
