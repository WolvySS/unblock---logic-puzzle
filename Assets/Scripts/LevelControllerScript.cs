﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelControllerScript : MonoBehaviour {

	public static LevelControllerScript instance = null;

	GameObject levelSign;
	int sceneIndex,levelPassed;

	public GameObject block;
	Vector3 FinalPos;

	void Start () {
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);

		levelSign = GameObject.Find ("LevelNumber");
		sceneIndex = SceneManager.GetActiveScene ().buildIndex;
		levelPassed = PlayerPrefs.GetInt ("LevelPassed");

		FinalPos = new Vector3 (0, -2, 0);

	}

	void Update()
	{
		if (block.transform.position == FinalPos)
		{
			LevelComplete();
		}
	}

	public void LevelComplete()
	{
		if (sceneIndex == 10)
			Invoke ("LoadMainMenu", 1f);
		else {
			if (levelPassed < sceneIndex)
				PlayerPrefs.SetInt ("LevelPassed", sceneIndex);
			levelSign.gameObject.SetActive (false);
			Invoke ("LoadNextLevel", 1f);
		}
	}
		
	void LoadNextLevel () {
		SceneManager.LoadScene (sceneIndex + 1);
	}

	void LoadMainMenu () {
		SceneManager.LoadScene ("Main Menu");
	}

}
